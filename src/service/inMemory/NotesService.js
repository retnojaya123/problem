const { nanoid } = require('nanoid');

class NotesService {
    constructor() {
        this._notes = [];
    }

    addNote({title, body, tags}) {
        const id = nanoid(16);
        const createAt = new Date().toISOString();
        const updateAt = createAt;

        const newNote = {
            title,tags,body,id,createAt,updateAt
        }
        this._notes.push(newNote);
        const isSucess = this._notes.filter((note) => note.id === id).length > 0;
            if(!isSucess){
                throw new error('catatan gagal ditambahkan');
            }
        return id
    }

    getNotes(){
        return this._notes;
    }

    getNotebyId(id) {

    }

    editNoteById(id, {title, body, tags}) {
        const index = this._notes.findIndex((note) => note.id === id);
            if(index === -1) {
                throw new Error (' gagal memperbarui catatan . id tidak ditemukan');
            }
        
        const updateAt = new Date().toISOString();
        this._notes[index] = {
            ...this._notes[index],
            title,
            tags,
            body,
            updateAt,
        };
    }
    
    deleteNotebyId(id) {
        const index = this._notes.findIndex((note) => note.id === id);
            if(index === -1) {
                throw new Error('catatan gagal dihapus . id tidak ditemukan');
            }
        this.notes.splice(index, 1);
    }
}

module.exports = NotesService;

